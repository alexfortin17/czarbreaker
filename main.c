#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

enum returnCode {SUCCESS = 0, FAILURE = -1};

struct integerReturn{
    int returnValue;
    int returnCode;
    char* returnMessage;
};

int real_modulo(int num, int modulo)
{
    int r = num % modulo;
    return r < 0 ? r + modulo : r;
}

char encrypt_letter(const int czarSpread, char letter){
    int letter_as_int = letter;
    int letter_in_alpha_order = letter_as_int - 97;
    return (char) (real_modulo(letter_in_alpha_order + czarSpread,  26) + 97);
}

char decrypt_letter(const int czarSpread, char letter){
    int letter_as_int = letter;
    int letter_in_alpha_order = letter_as_int - 97;
    return (char) (real_modulo(letter_in_alpha_order - czarSpread,  26) + 97);
}

struct integerReturn getCzarKey(const char* plaintext, const char* ciphertext){
    struct integerReturn ret;

    if (strlen(plaintext) != strlen(ciphertext)){
        ret.returnCode = FAILURE;
        ret.returnValue = -1;
        ret.returnMessage = "The two strings must have the same length\n";
        return ret;
    }
    int ptext_letterone = plaintext[0] - 97;
    int ctext_letterone = ciphertext[0] - 97;
    int spread = real_modulo(ctext_letterone - ptext_letterone, 26);
    int index_to_check = 1;
    for (int x = index_to_check; x < strlen(plaintext); x = x+1){
        if (encrypt_letter(spread, plaintext[x]) != ciphertext[x]){
            ret.returnCode = FAILURE;
            ret.returnValue = -1;
            ret.returnMessage = "The plaintext and ciphertext has a mix of different shifts. ERROR!!\n";
            return ret;
        }
    }
    ret.returnCode = SUCCESS;
    ret.returnValue = spread;
    ret.returnMessage = "The key has been found\n";
    return ret;
}


int main() {

    //Enter Plaintext here
    char plaintext[7] = "attack";
    //Enter Corresponding Ciphertest here
    char ciphertext[7] = "rkkrtb";

    struct integerReturn returnObj = getCzarKey(plaintext, ciphertext);

    if (returnObj.returnCode == SUCCESS) {
        printf("%s", returnObj.returnMessage);
        printf("%d", returnObj.returnValue);
        return SUCCESS;
    }
    else{
        return FAILURE;
    }

}